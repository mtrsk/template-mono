# Template pra Monografias ou trabalhos acadêmicos usando ABNTeX v2.0

## Build (Usando Nix):

* Com Direnv

```
$ direnv allow
$ make
```

* via `nix-shell`

```
$ nix-shell
$ make
```
