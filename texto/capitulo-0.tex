% ----------------------------------------------------------
%\chapter{Introdução}
\chapter{Prólogo}
% ----------------------------------------------------------

\cblkqt{Aristóteles formulou seus silogismos na antiguidade e William de Ockham estudou lógica na idade média. Contudo, a disciplina de lógica moderna começou com Gottlob Frege e seu Begriffschrift, escrito em 1879 quando o mesmo tinha 31 anos.}{Philip Wadler \cite{wadler2000proofs}}

\section{A lógica do séc.XIX e a crise do séc.XX}

Friedrich Ludwig Gottlob Frege (1848 - 1925) foi um matemático, lógico e filósofo alemão que trabalhou na Universidade de Jena. Fregue essencialmente reestruturou a disciplina da Lógica ao construir um sistema formal que deu origem ao cálculo de predicados. Em seu sistema formal, Frege desenvolveu a análise de proposições quantificadas e formalizou a noção de \qt{Prova} em termos que ainda são aceitos atualmente. Frege então demonstrou que seria possível utilizar este sistema para resolver proposições matemáticas teóricas em termos de noções lógicas e matemáticas mais simples. Um dos Axiomas que Frege adicionou ao seu sistema, na tentativa de derivar partes significantes da Matemática via Lógica, acabou sendo incosistente \cite{sep-frege}.

\begin{figure}[ht]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.75\linewidth]{intro/frege.png}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{intro/russell.png}
\end{subfigure}
\caption[ Frege e Russell ]
{Gottlob Frege e Bertrand Russell}
\end{figure}

O paradoxo foi uma descoberta independente do filósofo inglês Bertrand Russell e acabou demonstrando que o formalismo desenvolvido por Frege era, na verdade, inconsistente. Particurlamente o problema se encontrava no Axioma V, conhecido atualmente como compreensão irrestrita de conjuntos \cite{sep-russell-paradox}. O paradoxo de Russell se origina da ideia de que qualquer condição pode ser usada para determinar um conjunto:

\begin{enumerate}
    \item A notação da compreensão de conjuntos, denotando "O conjunto dos $x$ tal que $\varphi(x)$".
        \[ \{ x \mid \varphi(x) \} \]
    \item Seja $R$ o conjunto $\{x \mid \lnot \varphi(x) \}$, onde $\varphi(x) = x \in x $.
    \item $ R \in R $?
    \item Paradoxo:
        \begin{itemize}
            \item Se $R \in R$, então $R \not \in R$.
            \item Se $R \not \in R$, então $R \in R$.
        \end{itemize}
        ou seja:
        \[ R \in R \iff R \not \in R \]
\end{enumerate}

Em resposta ao paradoxo, Russell desenvolveu sua Teoria dos Tipos e, em coautoria com Alfred North Whitehead, embarcou no projeto monumental do Principia Mathematica. David Hilbert também expandiu seu programa de construção de uma base axiomática consistente para a matemática, de modo que incluísse uma base axiomática para a Lógica e a Teoria dos Conjuntos. Finalmente, Luitzen Brouwer desenvolveu o intuicionismo, cuja idéia básica é a de que não se pode afirmar a existência de um objeto matemático a menos que se possa definir um procedimento para construí-lo.

Juntos, todos esses projetos ajudaram a concentrar a atenção nas conexões entre lógica, linguagem e matemática. Eles também permitiram que os lógicos desenvolvessem uma consciência explícita da natureza dos sistemas formais e dos tipos de resultados metalógicos e metamatemáticos que provaram ser centrais para a pesquisa nos fundamentos da lógica e da matemática nos últimos cem anos \cite{sep-russell-paradox}.

A Ciência da Computação como disciplina nasceu dos formalismos desenvolvidos durante a crise dos fundamentos da matemática no século passado. A presença de conceitos lógicos ocupa uma parte central da disciplina, chegando a ser descrita como \qt{o cálculo da ciência da computação}\cite{manna1985logical}.

\newpage

\begin{figure}[htb]
\centering
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{intro/godel.png}
    \caption{Kurt Gödel}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.73\linewidth]{intro/gentzen.jpg}
    \caption{Gerhard Gentzen}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.78\linewidth]{intro/turing.png}
    \caption{Alan Turing}
\end{subfigure}

\medskip

\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{intro/church.jpg}
    \caption{Alonzo Church}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{intro/curry.jpg}
    \caption{Haskell B. Curry}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.82\linewidth]{intro/howard.jpg}
    \caption{William A. Howard}
\end{subfigure}

\medskip

\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=0.75\linewidth]{intro/brujin.jpg}
    \caption{Nicolaas de Bruijn}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=\linewidth]{intro/girard.jpg}
    \caption{Jean-Yves Girard}
\end{subfigure}\hfil
\begin{subfigure}{0.25\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{intro/reynolds.jpg}
    \caption{John C. Reynolds}
\end{subfigure}
\caption[ Gentzen, Gödel, Turing, Church, Curry, Howard, de Brujin, Girard]
{Gödel famosamente provou seus dois Teoremas da Incompletude. Gentzen desenvolveu o cálculo com Sequentes e a Dedução Natural. Turing introduziu seu formalismo para resolver o Entscheidungsproblem. Church formulou o $\lambda$-Cálculo e Teoria simples dos Tipos. Haskell Curry e William Howard observaram um isomorfismo entre Tipos/Proposições e Programas/Provas. N.G. de Brujin usou o isomorfismo de Curry-Howard para verificar computacionalmente teoremas matemáticos. Girard e Reynolds conceberam de forma independente o System F.}
\label{fig:big-logic-shrine}
\end{figure}
