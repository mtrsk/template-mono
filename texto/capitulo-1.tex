\chapter{Introdução}

Warren McCulloch e Walter Pitts em \cite{mcculloch1943logical} desenvolveram modelos para tentar entender como cérebro humano conseguia produzir padrões complexos a partir de células básicas conectadas. Neste trabalho é descrito um modelo bem simplificado de um neurônio, sendo um dos primeiros exemplos do que veio a ser chamado depois de \texttt{redes neurais}.

Stephen Kleene, um matemático americano e um dos estudantes de Alonzo Church, inspirado pelo trabalho de Warren e Pits descreveu estes modelos em \cite{kleene1951representation} com uma álgebra que chamou de \qt{conjuntos regulares}. A notação usada para expressar esses conjuntos ficou conhecida como \qt{expressões regulares}, os resultados de Kleene, porém, só seriam aplicados na computação com o surgimento do Unix.

\begin{figure}[ht]
\centering
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{cap1/warren.jpg}
    \caption{Warren McCulloch}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{cap1/pitts.jpg}
    \caption{Walter Pitts}
\end{subfigure}

\medskip

\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.5\linewidth]{cap1/kleene.jpg}
    \caption{Stephen Coole Kleene}
\end{subfigure}\hfil
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=0.6\linewidth]{cap1/thompson.jpg}
    \caption{Ken Thompson}
\end{subfigure}
\caption[McCulloch, Pitts, Kleene e Thompson]
{Entre as primeiras conjecturas de Warren e Pitts, até a primeira implementação por Thompson, se passaram mais de 20 anos}
\end{figure}

O Unix (oficialmente UNIX) é um sistema operacional multi-tarefa, multi-usuário que possui muitas variações ao longo do tempo. O Unix original foi desenvolvido nos laboratórios Bell da AT\&T por Ken Thompson, Dennis Ritchie, entre outros. Thompson publicou em 1968 \qt{Um Algoritmo para buscas via Expressões Regulares} \cite{thompson1968programming}, depois implementando a notação de Kleene no editor de texto padrão do Unix (\texttt{ed}). O trabalho de Thompson inspirou a criação de compiladores para expressões regulares e eventualmente levou à crianção to programa \texttt{grep} em 1973 e sua implementação mais completa (o \texttt{egrep}) em 1979, por Alfred Aho. Outras ferramentas do Unix, como o editor de streams \texttt{sed} e a linguagem \texttt{awk}, também provêm mecanismos para descrição de padrões via expressões regulares.


\begin{table}[ht]
\begin{center}
\begin{tabular}{ | m{2cm} || m{8cm} | }
\hline
Símbolo & Função\\ 
\hline
\texttt{.} & Match em qualquer caractere\\ 
\hline
\texttt{[chars]} & Match em qualquer caractere no escopo dos colchetes\\ 
\hline
\texttt{[\escmeta chars]} & Match em qualquer caractere que não está no escopo dos colchetes\\ 
\hline
\texttt{\escmeta} & Match no início da linha\\ 
\hline
\texttt{\$} & Match no fim de linha\\ 
\hline
\texttt{\bklash w} & Match em qualquer \qt{palavra}, i.e., \texttt{[a-zA-Z0-9\_]}\\ 
\hline
\texttt{\bklash s} & Match em caracteres que representam espaços em branco, i.e., \texttt{[~\bklash f\bklash t\bklash r\bklash n]}\\ 
\hline
\texttt{$~\mid~$} & Match no elemento da esquerda ou da direita\\ 
\hline
\texttt{(expr)} & Agrupa elementos\\ 
\hline
\texttt{?} & Zero ou uma instância do elemento que a precede\\ 
\hline
\texttt{$\ast$} & Zero ou mais instâncias do elemento que a precede\\ 
\hline
\texttt{$+$} & Um ou mais instâncias do elemento que a precede\\ 
\hline
\texttt{$\{ n \}$} & Match em exatas n instâncias de elementos que a precede\\ 
\hline
\texttt{$\{ \min, \}$} & Match em no mínimo n instâncias\\ 
\hline
\texttt{$\{ \min, \max\}$} & Match em qualquer quantidade de instâncias entre $\min$ e $\max$\\ 
\hline
\end{tabular}
\end{center}
\caption{Tabela com metacarateres usados em expressões regulares nos Unixes \cite{nemeth2017unix}.}
\end{table}

\section{Alfabetos, Linguagens e Strings}

Um alfabeto é um conjunto finito (e não vazio) de caracteres, usualmente denotado por $\Sigma$. Os elementos de $\Sigma$ são chamados de \qt{símbolos}. Alguns exemplos comuns de alfabetos são:

\begin{itemize}
    \item $\Sigma_{L} = \{a, b, c, \ldots, z, A, B, \ldots, Z\}$ é o alfabeto latino padrão.
    \item $\Sigma_{2} = \{0, 1\}$ é um alfabeto binário.
    \item O conjunto de todos os caracteres \texttt{ASCII}.
\end{itemize}

Uma \texttt{string} (ou \texttt{palavra}) é qualquer sequência finita de símbolos extraídos de um alfabeto específico. Por exemplo, \qt{010101111} é uma string gerada pelos símbolos do alfabeto $\Sigma = \{0,1\}$, a string \qt{1111} também pode ser gerada pelo mesmo alfabeto.

\subsection{Operações em strings}

Uma operação útil quando se lida com strings é poder computar seu comprimento. O \textit{comprimento} é definido pela quantidade de posições pros símbolos de uma string, vale ressaltar que isso não deve ser confundido com a quantidade de símbolos. Por exemplo, a string \qt{1010} possui apenas $2$ símbolos ($0$ e $1$), mas seu comprimento, i.e. a quantidade de posições para estes símbolos, é $4$. A notação padrão para o comprimento de uma string $w$ qualquer é $\len{w}$.

Outra operação usual é poder tomar duas strings arbitrárias $x, y$ pertencentes a um alfabeto $\Sigma$ e poder concatená-las. A \textit{concatenação} de duas strings $x,y$ formadas de símbolos de $\Sigma$, denotada por $xy$, representa a composição dos símbolos de $x = a_1 a_2 \ldots a_{k-1} a_{k}$ com os símbolos de $y = b_1 b_2 \ldots b_{l - 1} b_l$, formando $xy = a_1 a_2 \ldots a_{k-1} a_k b_1 b_2 \ldots b_{l-1} b_l$ de comprimento $k + l$.

Com as definições anteriores é possível trabalhar com a noção de uma \qt{string vazia}, usualmente denotada por $\varepsilon$, $\varepsilon$ é resultado da escolha de $0$ símbolos de qualquer alfabeto $\Sigma$. A string vazia possui propriedades interessantes como:

\begin{itemize}
    \item $\varepsilon$ tem comprimento $0$.
        \[ \len{\varepsilon} = 0 \]
    \item $\varepsilon$ é o elemento neutro da concatenação.
        \[ \varepsilon w = w = w \varepsilon \]
\end{itemize}

Se $\Sigma$ é um alfabeto, pode-se expressar o conjunto de todas as strings com um determinado comprimento usando a notação de exponenciação. Define-se $\Sigma^k$ como o conjunto das strings geradas por $\Sigma$ que possuem comprimento $k$. Por exemplo, dado $\Sigma = \{0,1\}$, tem-se:

\begin{itemize}
    \item $\Sigma^0 = \{ \varepsilon \} $
    \item $\Sigma^1 = \{ 0, 1\} $
    \item $\Sigma^2 = \{ 00, 01, 10, 11 \} $
    \item $\Sigma^3 = \{ 000, 001, 010, 100, 101, 011, 110, 111 \} $
\end{itemize}

O conjunto de todas as strings em um alfabeto $\Sigma$ é denotado por $\kleene{\Sigma}$, se a string vazia for desconsiderada usa-se $\kplus{\Sigma}$. Ambos os casos são representandos por uma união infinita:

\begin{align}
    \kleene{\Sigma} &= \Sigma^0 \cup \Sigma^1 \cup \Sigma^2 \cup \cdots\\
    \kplus{\Sigma} &= \Sigma^1 \cup \Sigma^2 \cup \Sigma^3 \cup \cdots
\end{align}

\subsection{Linguagens Regulares}

Uma linguagem $\mathcal{L}$ sob um alfabeto $\Sigma$ é um conjunto $\mathcal{L} \subseteq \Sigma$. Alguns exemplos abstratos incluem:

\begin{itemize}
    \item $\kleene{\Sigma}$ é uma linguagem sob qualquer alfabeto $\Sigma$.
    \item O conjunto $\{\varepsilon, 01, 10, 0011, 0011, 0101, 1010, \ldots \}$ é uma linguagem sob $\Sigma = \{0,1\}$ que contém todas as strings de $ \kleene{\Sigma} $ com uma quantidade igual de zeros e uns.
    \item O conjunto $\{\varepsilon\}$ consiste apenas da string vazia e é uma linguagem sob qualquer alfabeto $\Sigma$.
    \item A linguagem $\mathcal{D}$ sob of alfabeto $\Sigma = \{ 0, 1\}$ contém todas as sequências infinitas formadas por $0$ e $1$.
    \[ \mathcal{D} = \{ \omega_0 \omega_1 \omega_2 \ldots \mid \omega_i \in \{0, 1\}, \forall i \in \mbb{N} \}\]
\item $\emptyset \subset \kleene{\Sigma}$ é uma linguagem sob qualquer alfabeto $\kleene{\Sigma}$. A \textit{linguagem vazia} $\emptyset$ não contém nenhuma string e não deve ser confundida com $\{ \varepsilon \}$, que contém apenas uma string.
\end{itemize}
adicionalmente, $\mathcal{L} \subset \kleene{\Sigma}$ é dita ser \textit{regular} se obedece a um dos casos \cite{sipser2006introduction}:
\begin{itemize}
    \item $\mathcal{L} = \emptyset$.
    \item $\mathcal{L} = \{\varepsilon\}$.
    \item $\mathcal{L} = \{c\}$, onde $c \in \Sigma$.
    \item $\mathcal{L}$ pode ser construída a partir das seguintes operações:
    \begin{itemize}
        \item A \textit{concatenação} de duas linguagens regulares $L_1$ e $L_2$ sob um alfabeto $\Sigma$ é denotada por:
            \[ L_1 \conc L_2 = \{ wx \in \kleene{\Sigma} \mid w \in L_1 \land x \in L_2 \} \]
            a notação $L^n$, onde $n \geq 0$, representa o seguinte conjunto:
            \[ L^n = \underbrace{L \conc L \conc \cdots \conc L \conc L}_{\text{n vezes}} = \{ w_0 w_1 \ldots w_{n-1} w_{n} \in \kleene{\Sigma} \mid \forall w_i \in L \} \]
            também é possível omitir a operação \qt{$\conc$}, deixando-a implícita, por exemplo:
            \[ L_1 \conc L_2 \equiv L_1 L_2 \]
        \item A \textit{união} de duas linguagens regulares $L_1$ e $L_2$ sob um alfabeto $\Sigma$ é denotada por:
            \[ L_1 \union L_2 = \{ y \in \kleene{\Sigma} \mid y \in L_1 \lor y \in L_2 \} \]
        \item O \textit{fecho de Kleene} de uma linguagem regular $L$, denotado por $\kleene{L}$, representa a seguinte união infinita:
            \[ \kleene{L} = \bigcup_{i=0}^{\infty} L^i \]
    \end{itemize}
\end{itemize}

Na aritmética, podemos usar as operações $+$ e $\times$ para definir expressões como \qt{$(5+3)\times 4$} que, quando computada, tem valor $32$. Similarmente podemos usar o formalismo de Kleene para definir expressões que descrevem linguagens. Expressões regulares são formas compactas de escrever linguagens regulares. Por exemplo, a expressão \qt{$(0 \cup 1) \kleene{0}$} descreve uma linguagem regular $\mathcal{L}$ tal que:

\[ \mathcal{L} = \{0, 1, 00, 10, 000, 100, \ldots \} \]
que contém todos as strings de $\Sigma$ que possuem uma quantidade igual de zeros e uns.

%\subsection{Semigrupos e Monóides}
%
%Um dos tipos mais simples de estruturas algébricas são os \textit{semigrupos} e \textit{monóides}, esta seção é uma adaptação de \cite{pin2010mathematical} e busca investigar a definição de strings sob a ótica da álgebra abstrata. Um semigrupo é um conjunto $S$ dotado de uma operação binária fechada ($S \times S \rightarrow S$) e associativa, por conveniência chamada de multiplicação. Exemplos de semigrupos são:
%
%\begin{itemize}
%    \item Os inteiros positivos ($\mbb{Z}^+$) munidos da operação de adição: $(\mbb{Z}^+, +)$.
%    \item O conjunto com valores booleanos $\mbb{B} = \{\top, \bot\}$ munido com as operações de disjunção ou conjunção: $(\mbb{B}, \lor)$ ou $(\mbb{B}, \land)$.
%\end{itemize}
%
%\begin{itemize}
%    \item Os inteiros positivos ($\mbb{Z}^+$) munidos da operação de adição, tendo $0$ como elemento neutro: $(\mbb{Z}^+, +, 0)$.
%    \item O conjunto com valores booleanos $\mbb{B} = \{\top, \bot\}$ munido com as operações de disjunção ou conjunção, tendo $\bot$ e $\top$ como elementos neutros das respectivas operações: $(\mbb{B}, \lor, \bot)$ ou $(\mbb{B}, \land, \top)$.
%\end{itemize}

\section{A Play with Regular Expressions}

A \textit{functional pearl}\footnote{Séries de artigos demonstrando utilizações elegantes do Paradigma Funcional. São escritos de forma a ensinar técnicas importantes de programação e princípios de design.} \qt{A Play on Regular Expressions}\cite{fischer2010play} se divide em três atos, imitando a estrutura de uma peça teatral. O primeiro divide-se em duas cenas, o segundo e terceiro, em três cenas, totalizando oito cenas. Cada cena descreve uma implementação diferente para expressões regulares, aumentando gradualmente em nível de complexidade e generalidade.

Ao final do artigo tem-se uma implementação elegante em Haskell, o algoritmo em questão não apenas conseguia competir em termos de eficiência com implementações profissionais em C\texttt{++}, mas também é genérico o suficiente para resolver problemas como encontrar a correspondência mais longa a esquerda e contagem de correspondências em um $\texttt{regex}$.

\section{Testes baseados em propriedades}

Testes são uma das maneiras mais básicas se garantir a robustez de um software, sendo assim a qualidade do software pode estar atrelada a quão bem escritos estão seus testes. Uma vantagem de se construir programas com funções puras é que testá-las se torna algo menos laborioso, visto que não é mais necessário se preocupar com o estado antes e depois de sua execução. Mesmo que todas essas facilidades, escrever testes continua sendo algo difícil, pois para cara $n$ novas funcionalidades a quantidade de testes escritos cresce na seguinte ordem:

\begin{enumerate}
    \item $\mathcal{O}(n)$ se as $n$ funcionalidades forem independentes.
    \item $\mathcal{O}(n^2)$ se cada funcionalidade depende de outra.
    \item $\mathcal{O}(n^3)$ se cada funcionalidade depende des outras três.
\end{enumerate}
e assim sucessivamente, cada nível de dependência aumenta de forma considerável a quantidade de testes.

John Hughes e Koes Clessen descrevem em \cite{claessen2011quickcheck} que o custo para escrita de testes motiva esforços para automatizá-los. O resultado de seu trabalho conjunto foi o QuickCheck, uma biblioteca em Haskell para geração de testes aleatórias a partir de propriedades. Neste tipo de ferramenta o programador deve definir a especificação de um programa, através da criação de propriedades que devem ser satisfeitas (usando combinadores disponibilizados pela biblioteca), que são então usados pelo QuickCheck para fazer para gerar analisar a distribuição dos dados, gerar automaticamente testes e , quando encontrado um contra-exemplo à propriedade, minimizá-lo de forma a encontrar um contra-exemplo mínimo (e mais fácil de ser entendido).

\subsection{Coq e QuickChick}

O assistente de provas Coq \cite{the_coq_development_team_2019_2554024} foi criado para desenvolver provas matemáticas, escrever especificações formais de programas e verificar sua corretude com respeito à especificação. A linguagem usada para escrever as provas chama-se Gallina\footnote{Um trocadilho com Coq, que significa \qt{Galo} em francês}. Os termos dessa linguagem podem representar programas, propriedades e provas de propriedades. Usando o Isomorfismo de Curry-Howard, programas, propriedaes e provas podem ser formalizadas na mesma linguagem, conhecida como \qt{Cálculo de Construções Indutivas}, trata-se de um $\lambda$-cálculo tipado de ordem superior, inicialmente investigado em \cite{coquand1986calculus}. Alguns projetos de notoriedade feitos em Coq incluem:

\begin{itemize}
    \item CompCert \cite{leroy2012compcert}: Um compilador para um subconjunto da linguagem C formalmente verificado, o código gerado em assembly tem garantias de seguir o comportamento descrito pelo código fonte em C.
    \item Uma prova formal do teorema das 4 cores \cite{gonthier2008formal}.
    \item CertiKOS \cite{gu2011certikos}: Uma arquitetura para construção de kernels concorrentes.
\end{itemize}

Devido ao sucesso do QuickCheck outras linguagens começaram a implementá-lo. Recentemente o Coq ganhou seu próprio framework para geração de testes baseados em propriedades, o QuickChick \cite{denes2014quickchick}, que implementa o protocolo original do QuickCheck.

\section{Objetivos}

\subsection{Geral}

\begin{itemize}
    \item Investigar os fundamentos matemáticos da criação de software correto por construção, estudando referências como a série \qt{Software Foundations}\cite{pierce2010software}, \qt{Coq'Art}\cite{bertot2013interactive} e \qt{Certified Programming with Dependent Types}\cite{chlipala2011certified}.
\end{itemize}

\subsection{Específicos}

\begin{itemize}
    \item Implementar o algoritmo descrito no artigo \qt{A play on regular expressions}.
    \item Usar a assistente de provas Coq  para fazer a especificação formal do algoritmo.
    \item Fazer a extração da especificação em Coq.
    \item Verificar a robustez da implementação usando a técnica de testes baseados em propriedades em QuickChick.
\end{itemize}
