with import <nixpkgs> {};

let
  texliveEnv = texlive.combine {
    inherit (texlive) scheme-basic latexmk collection-langportuguese;
  };
in
stdenv.mkDerivation rec {
  name = "latex-env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    tectonic
    texliveEnv
  ];
}
